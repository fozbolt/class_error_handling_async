class Shape{
    constructor(x, y){
        this.x = x
        this.y = y
        this.perimeterValue = undefined
        this.areaValue = undefined
    }

    perimeter(){
        this.perimeterValue = 2*(this.x + this.y)
        }
    

    area(){
        this.areaValue = this.x * this.y
    }

}


class Rectangle extends Shape{
    perimeter(){
        super.perimeter()
        console.log(`Perimeter of retangle: ${this.perimeterValue}`)
        
    }
    area(){
        super.area()
        console.log(`Area of retangle: ${this.areaValue}`)
        
    }
}

let rect = new Rectangle(5, 4)
rect.perimeter()
rect.area()


class Square extends Shape{
    perimeter(){
        super.perimeter()
        console.log(`Perimeter of square: ${this.perimeterValue}`)
        
    }
    area(){
        super.area()
        console.log(`Area of square: ${this.areaValue}`)
        
    }
}

let sq = new Square(5, 5)
sq.perimeter()
sq.area()


class Circle extends Shape{

    constructor(radius){
        super(null, null)
        this.radius = radius
    }
    perimeter(){
            this.perimeterValue = 2 * 3.14 * this.radius
            console.log(`Perimeter of circle: ${this.perimeterValue}`)
    }

    area(){
        this.areaValue =  3.14 * this.radius**2
        console.log(`Area of circle: ${this.areaValue}`)
    }
}

let cir = new Circle(3)
cir.perimeter()
cir.area()