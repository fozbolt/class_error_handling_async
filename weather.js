const axios = require('axios').default;


class WeatherInfo {
  
    constructor(city) {
      this.city = city;
    }
  
    async getForecast() {
      
        try{
            this.forecast = await axios.get('http://api.weatherapi.com/v1/current.json?key=fa44bf5be6134bd79eb200616210408&q=' + this.city + '&aqi=no')
            console.log(this.forecast.data)

        }catch{
            console.log('Error accured while fetching weather data')
        }
       
    }
  
  }
  

let weatherRijeka = new WeatherInfo('Rijeka');
weatherRijeka.getForecast()

let weatherZagreb = new WeatherInfo('Zagreb');
weatherZagreb.getForecast()















